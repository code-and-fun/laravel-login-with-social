@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div id='digital-clock'>
                <span id='hour'></span><span class='red-dot'>:</span><span id='min'></span><span class='red-dot'>:</span><span id='second'></span>
            </div>
        </div>
        <div class="col-md-8 mt-4">
            <img src="{{url('https://i1.wp.com/supportdriven.com/wp-content/uploads/2017/10/github-logo.png')}}" alt="" class="img-fluid">
        </div>
    </div>
</div>

<script>
    function showclock() {
        let today = new Date();
        let hours = today.getHours();
        let mins = today.getMinutes();
        let seconds = today.getSeconds();
        const addZero = num => {
            if(num < 10) { return '0' + num };
            return num;
        }
        $('#hour').text(addZero(hours));
        $('#min').text(addZero(mins));
        $('#second').text(addZero(seconds));
    }
    setInterval(showclock, 1000);
</script>
@endsection

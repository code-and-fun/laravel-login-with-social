<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('{social}/login', 'Auth\LoginController@redirectToSocial')->name('login.social');
Route::get('{social}/callback', 'Auth\LoginController@handleSocialCallback')->name('login.social.callback');
